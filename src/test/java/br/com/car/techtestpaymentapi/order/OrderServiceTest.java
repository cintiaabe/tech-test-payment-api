package br.com.car.techtestpaymentapi.order;

import br.com.car.techtestpaymentapi.domain.order.*;
import br.com.car.techtestpaymentapi.domain.product.Product;
import br.com.car.techtestpaymentapi.domain.product.ProductDTO;
import br.com.car.techtestpaymentapi.domain.product.ProductRepository;
import br.com.car.techtestpaymentapi.domain.seller.Seller;
import br.com.car.techtestpaymentapi.domain.seller.SellerDTO;
import br.com.car.techtestpaymentapi.domain.seller.SellerRepository;
import br.com.car.techtestpaymentapi.exception.OrderException;
import br.com.car.techtestpaymentapi.exception.OrderNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private SellerRepository sellerRepository;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void whenOrderSendThenItShouldBeCreated() throws Exception {

        OrderDTO orderDTO = buildOrderDTO();
        Seller expectedSeller = buildSeller();
        Product expectedProduct = buildProduct();

        // when
        when(sellerRepository.findById(orderDTO.getSellerDTO().getId())).thenReturn(Optional.of(expectedSeller));
        when(productRepository.findAllById(List.of(orderDTO.getOrderItemDTOList().get(0).getProductDTO().getId())))
                .thenReturn(List.of(expectedProduct));
        when(orderRepository.save(any())).then(invocation -> invocation.getArguments()[0]);

        // then
        Order resultOrder = orderService.insert(orderDTO);

        //asserts
        assertThat(resultOrder.getSeller().getName(), is(equalTo(expectedSeller.getName())));
        assertThat(resultOrder.getOrderItems().get(0).getProduct().getDescription(), is(equalTo(expectedProduct.getDescription())));
        assertThat(resultOrder.getOrderItems().get(0).getQuantity(),is(equalTo(orderDTO.getOrderItemDTOList().get(0).getQuantity())));
        assertThat(resultOrder.getDate(),is(equalTo(orderDTO.getDate())));
        assertThat(resultOrder.getStatus(),is(equalTo(orderDTO.getStatus())));
    }

    @Test
    public void whenSendOrderWithoutItemThenThrowAnException() {
        OrderDTO orderDTO = buildOrderDTOWithoutItem();

        //asserts
        OrderException exception = Assertions
                .assertThrows(OrderException.class, () -> orderService.insert(orderDTO));
        Assertions.assertEquals("Order must have almost one item", exception.getMessage());
    }

    @Test
    public void whenSendOrderWithSellerDoesNotExistThenThrowException() {
        OrderDTO orderDTO = buildOrderDTOWithNonExistentSeller();

        // when
        when(sellerRepository.findById(orderDTO.getSellerDTO().getId())).thenReturn(Optional.empty());


        //asserts
        OrderException exception = Assertions
                .assertThrows(OrderException.class, () -> orderService.insert(orderDTO));
        Assertions.assertEquals("Seller does not exist", exception.getMessage());
    }

    @Test
    public void whenSendOrderWithProductDoesNotExistThenThrowException() {
        OrderDTO orderDTO = buildOrderDTOWithNonExistentProduct();
        Seller expectedSeller = buildSeller();

        // when
        when(sellerRepository.findById(orderDTO.getSellerDTO().getId())).thenReturn(Optional.of(expectedSeller));
        when(productRepository.findAllById(any())).thenReturn(List.of());

        //asserts
        OrderException exception = Assertions
                .assertThrows(OrderException.class, () -> orderService.insert(orderDTO));
        Assertions.assertEquals("Product does not exist", exception.getMessage());
    }

    @Test
    public void whenUpdateOrderThenItShouldBeUpdated() {
        Order expectedOrder = buildOrder();

        Long id = 1L;
        OrderStatusEnum newStatus = OrderStatusEnum.PAYMENT_ACCEPTED;

        // when
        when(orderRepository.findById(id)).thenReturn(Optional.of(expectedOrder));
        when(orderRepository.save(any())).then(invocation -> invocation.getArguments()[0]);

        // then
        Order updatedOrder = orderService.update(id,newStatus);

        //asserts
        assertThat(updatedOrder.getStatus(), is(equalTo(expectedOrder.getStatus())));
    }

    @Test
    public void whenUpdateOrderWithNonValidIdThenThrowException(){
        Order expectedOrder = buildOrder();

        Long id = 5L;
        OrderStatusEnum newStatus = OrderStatusEnum.PAYMENT_ACCEPTED;

        // when
        when(orderRepository.findById(id)).thenReturn(Optional.empty());

        //asserts
        OrderNotFoundException exception = Assertions
                .assertThrows(OrderNotFoundException.class, () -> orderService.update(id, newStatus));
        Assertions.assertEquals("Order does not exist with id " + id, exception.getMessage());
    }

    @Test
    public void whenUpdateOrderWithNonValidStatusThenThrowException() {
        Order expectedOrder = buildOrder();

        Long id = 1L;
        OrderStatusEnum newStatus = OrderStatusEnum.SHIPPED;

        // when
        when(orderRepository.findById(id)).thenReturn(Optional.of(expectedOrder));

        //asserts
        OrderException exception = Assertions
                .assertThrows(OrderException.class, () -> orderService.update(id, newStatus));
        Assertions.assertEquals("Invalid Status to update", exception.getMessage());
    }

    @Test
    public void whenFindByIdIsCalledThenReturnOrder() {
        Order expectedOrder = buildOrder();

        Long id = 1L;

        // when
        when(orderRepository.findById(id)).thenReturn(Optional.of(expectedOrder));
        Optional<Order> resultOrder = orderService.findById(id);
        verify(orderRepository, times(1)).findById(id);

    }

    private Product buildProduct() {
        Product product = new Product();
        product.setId(1L);
        product.setDescription("Brigadeiro Belga Teste");
        product.setPrice(230.00);
        return product;
    }

    private Seller buildSeller() {
        Seller seller = new Seller();
        seller.setId(1L);
        seller.setName("Cintia Teste");
        return seller;
    }

    private ProductDTO buildProductDTO() {
        return new ProductDTO(1L, "Brigadeiro Belga", 230.00);
    }

    private SellerDTO buildSellerDTO() {
        return new SellerDTO(1L, "Cintia");
    }

    private OrderItemDTO buildOrderItemDTO() {
        ProductDTO productDTO = buildProductDTO();

        return new OrderItemDTO(1L, productDTO, 1.0);
    }

    private OrderDTO buildOrderDTO() {
        SellerDTO sellerDTO = buildSellerDTO();

        OrderItemDTO orderItemDTO = buildOrderItemDTO();
        return new OrderDTO(1L, sellerDTO, LocalDate.of(2023, 01, 20),
                OrderStatusEnum.AWATING_PAYMENT, List.of(orderItemDTO));
    }

    private OrderDTO buildOrderDTOWithoutItem() {
        SellerDTO sellerDTO = buildSellerDTO();

        return new OrderDTO(1L, sellerDTO, LocalDate.of(2023, 01, 20),
                OrderStatusEnum.AWATING_PAYMENT, List.of());
    }

    private OrderDTO buildOrderDTOWithNonExistentSeller() {
        OrderItemDTO orderItemDTO = buildOrderItemDTO();
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(1L);
        orderDTO.setSellerDTO(new SellerDTO());
        orderDTO.setDate(LocalDate.of(2023,01,20));
        orderDTO.setStatus(OrderStatusEnum.AWATING_PAYMENT);
        orderDTO.setOrderItemDTOList(List.of(orderItemDTO));
        return orderDTO;
    }

    private OrderDTO buildOrderDTOWithNonExistentProduct() {
        OrderItemDTO orderItemDTO = buildOrderItemDTOWithNonExistentProduct();
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(1L);
        orderDTO.setSellerDTO(new SellerDTO());
        orderDTO.setDate(LocalDate.of(2023,01,20));
        orderDTO.setStatus(OrderStatusEnum.AWATING_PAYMENT);
        orderDTO.setOrderItemDTOList(List.of(orderItemDTO));
        return orderDTO;
    }

    private OrderItemDTO buildOrderItemDTOWithNonExistentProduct() {
        OrderItemDTO orderItemDTO = new OrderItemDTO();
        orderItemDTO.setId(1L);
        orderItemDTO.setProductDTO(new ProductDTO());
        orderItemDTO.setQuantity(2.0);
        return orderItemDTO;
    }

    private Order buildOrder() {
        Seller seller = buildSeller();

        OrderItem orderItem = buildOrderItem();
        Order order = new Order();
        order.setId(1L);
        order.setSeller(seller);
        order.setDate(LocalDate.of(2023,01,20));
        order.setStatus(OrderStatusEnum.AWATING_PAYMENT);
        order.setOrderItems(List.of(orderItem));
        return order;
    }

    private OrderItem buildOrderItem() {
        Product product = buildProduct();
        OrderItem orderItem = new OrderItem();
        orderItem.setId(1L);
        orderItem.setProduct(product);
        orderItem.setQuantity(1.0);
        return orderItem;
    }
}
