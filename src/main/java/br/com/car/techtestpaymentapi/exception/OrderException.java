package br.com.car.techtestpaymentapi.exception;

public class OrderException extends RuntimeException {
    public OrderException(String message){
        super(message);
    }
}
