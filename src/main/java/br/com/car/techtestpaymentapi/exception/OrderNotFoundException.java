package br.com.car.techtestpaymentapi.exception;

public class OrderNotFoundException extends RuntimeException {

    public OrderNotFoundException(Long id) {
        super(String.format("Order does not exist with id %s", id));
    }
}
