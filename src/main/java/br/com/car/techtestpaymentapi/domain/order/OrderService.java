package br.com.car.techtestpaymentapi.domain.order;

import br.com.car.techtestpaymentapi.domain.product.Product;
import br.com.car.techtestpaymentapi.domain.product.ProductRepository;
import br.com.car.techtestpaymentapi.domain.seller.Seller;
import br.com.car.techtestpaymentapi.domain.seller.SellerRepository;
import br.com.car.techtestpaymentapi.exception.OrderException;
import br.com.car.techtestpaymentapi.exception.OrderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository repository;

    @Autowired
    private OrderItemRepository itemRepository;

    @Autowired
    private SellerRepository sellerRepository;

    @Autowired
    private ProductRepository productRepository;

    public Optional<Order> findById (Long id){
        return repository.findById(id);
    }

    public Order insert (OrderDTO orderDTO){
        if ((orderDTO.getOrderItemDTOList() == null) || orderDTO.getOrderItemDTOList().isEmpty()) {
            throw new OrderException("Order must have almost one item");
        }
        Seller seller = sellerRepository.findById(orderDTO.getSellerDTO().getId())
                .orElseThrow(() -> new OrderException("Seller does not exist"));
        Order order = new Order();
        order.setSeller(seller);

        List<Long> productIds = orderDTO.getOrderItemDTOList().stream()
                .map(orderItemDTO -> orderItemDTO.getProductDTO().getId())
                .distinct()
                .collect(Collectors.toList());
        Map<Long, Product> productMap = productRepository
                .findAllById(productIds)
                .stream()
                .collect(Collectors.toMap(Product::getId, Function.identity()));
        List<OrderItem> orderItemList = new ArrayList<>();
        orderDTO.getOrderItemDTOList().forEach(orderItemDTO -> {
            OrderItem orderItem = new OrderItem();
            orderItem.setQuantity(orderItemDTO.getQuantity());
            Product product = Optional.ofNullable(productMap.get(orderItemDTO.getProductDTO().getId()))
                    .orElseThrow(() -> new OrderException("Product does not exist"));
            orderItem.setProduct(product);
            orderItem.setOrder(order);
            orderItemList.add(orderItem);
        });
        order.setOrderItems(orderItemList);
        order.setDate(orderDTO.getDate());
        order.setStatus(orderDTO.getStatus());
        return repository.save(order);
    }

    public Order update (Long id, OrderStatusEnum newStatus){
        Order orderUpdate = repository.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(id));
        if (!verifyStatus(orderUpdate.getStatus(),newStatus)) {
            throw new OrderException("Invalid Status to update");
        }
        orderUpdate.setStatus(newStatus);
        return repository.save(orderUpdate);
    }

    private boolean verifyStatus (OrderStatusEnum status, OrderStatusEnum newStatus){
        boolean result;
        switch (status){
            case AWATING_PAYMENT:
                result = (newStatus == OrderStatusEnum.PAYMENT_ACCEPTED) || (newStatus == OrderStatusEnum.CANCELED);
                break;
            case PAYMENT_ACCEPTED:
                result = (newStatus == OrderStatusEnum.SHIPPED) || (newStatus == OrderStatusEnum.CANCELED);
                break;
            case SHIPPED:
                result = newStatus == OrderStatusEnum.ARRIVED;
                break;
            default:
                result = false;
                break;
        }
        return result;
    }
}
