package br.com.car.techtestpaymentapi.domain.order;

public enum OrderStatusEnum {
    AWATING_PAYMENT,
    PAYMENT_ACCEPTED,
    SHIPPED,
    CANCELED,
    ARRIVED;
}
