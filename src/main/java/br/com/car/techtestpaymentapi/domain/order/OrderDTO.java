package br.com.car.techtestpaymentapi.domain.order;

import br.com.car.techtestpaymentapi.domain.seller.SellerDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

public class OrderDTO {
    private Long id;

    @NotNull @Valid
    private SellerDTO sellerDTO;
    @NotNull(message = "date is required")
    private LocalDate date;
    private OrderStatusEnum status;

    @Valid
    private List<OrderItemDTO> orderItemDTOList;

    public OrderDTO (){}

    public OrderDTO(Long id, SellerDTO sellerDTO, LocalDate date, OrderStatusEnum status, List<OrderItemDTO> orderItemList) {
        this.id = id;
        this.sellerDTO = sellerDTO;
        this.date = date;
        this.status = status;
        this.orderItemDTOList = orderItemList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SellerDTO getSellerDTO() {
        return sellerDTO;
    }

    public void setSellerDTO(SellerDTO sellerDTO) {
        this.sellerDTO = sellerDTO;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public OrderStatusEnum getStatus() {
        return status;
    }

    public void setStatus(OrderStatusEnum status) {
        this.status = status;
    }

    public List<OrderItemDTO> getOrderItemDTOList() {
        return orderItemDTOList;
    }

    public void setOrderItemDTOList(List<OrderItemDTO> orderItemDTOList) {
        this.orderItemDTOList = orderItemDTOList;
    }
}
