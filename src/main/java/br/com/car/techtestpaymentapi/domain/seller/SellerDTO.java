package br.com.car.techtestpaymentapi.domain.seller;

import javax.validation.constraints.NotNull;

public class SellerDTO {

    @NotNull(message = "seller id is required")
    private Long id;
    private String name;

    public SellerDTO(){
    }

    public SellerDTO(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
