package br.com.car.techtestpaymentapi.domain.order;

import br.com.car.techtestpaymentapi.domain.product.Product;
import br.com.car.techtestpaymentapi.domain.product.ProductDTO;
import br.com.car.techtestpaymentapi.domain.seller.Seller;
import br.com.car.techtestpaymentapi.domain.seller.SellerDTO;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class OrderMapper {

    public Order mapToEntity (OrderDTO orderDTO){
        Order order = new Order();
        order.setDate(orderDTO.getDate());
        Seller seller = new Seller();
        seller.setName(orderDTO.getSellerDTO().getName());
        order.setSeller(seller);
        order.setStatus(orderDTO.getStatus());
        order.setOrderItems(orderDTO.getOrderItemDTOList().stream()
                .map(orderItemDTO -> {
                    OrderItem orderItem = new OrderItem();
                    Product product = new Product();
                    product.setDescription(orderItemDTO.getProductDTO().getDescription());
                    product.setPrice(orderItemDTO.getProductDTO().getPrice());
                    orderItem.setProduct(product);
                    orderItem.setQuantity(orderItemDTO.getQuantity());
                    return orderItem;
                }).collect(Collectors.toList()));
        return order;
    }

    public OrderDTO mapToDto (Order order){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setId(order.getId());
        orderDTO.setDate(order.getDate());
        SellerDTO sellerDTO = new SellerDTO();
        sellerDTO.setId(order.getSeller().getId());
        sellerDTO.setName(order.getSeller().getName());
        orderDTO.setSellerDTO(sellerDTO);
        orderDTO.setStatus(order.getStatus());
        orderDTO.setOrderItemDTOList(order.getOrderItems().stream()
                .map(orderItem -> {
                    OrderItemDTO orderItemDTO = new OrderItemDTO();
                    orderItemDTO.setId(orderItem.getId());
                    ProductDTO productDTO = new ProductDTO();
                    productDTO.setId(orderItem.getProduct().getId());
                    productDTO.setDescription(orderItem.getProduct().getDescription());
                    productDTO.setPrice(orderItem.getProduct().getPrice());
                    orderItemDTO.setProductDTO(productDTO);
                    orderItemDTO.setQuantity(orderItem.getQuantity());
                    return orderItemDTO;
                }).collect(Collectors.toList()));
        return orderDTO;
    }
}
