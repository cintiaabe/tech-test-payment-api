package br.com.car.techtestpaymentapi.domain.order;

import br.com.car.techtestpaymentapi.domain.product.ProductDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class OrderItemDTO {

    private Long id;
    @NotNull(message = "product is required") @Valid
    private ProductDTO productDTO;
    @NotNull(message = "order item quantity is required")
    private Double quantity;

    public OrderItemDTO(){

    }

    public OrderItemDTO(Long id, ProductDTO productDTO, Double quantity) {
        this.id = id;
        this.productDTO = productDTO;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
