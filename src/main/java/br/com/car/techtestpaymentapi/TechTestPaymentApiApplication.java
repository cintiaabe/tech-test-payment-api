package br.com.car.techtestpaymentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechTestPaymentApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechTestPaymentApiApplication.class, args);
	}

}
