package br.com.car.techtestpaymentapi.api.order;

import br.com.car.techtestpaymentapi.domain.order.*;
import br.com.car.techtestpaymentapi.exception.OrderNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/order")
public class OrderController {

    @Autowired
    private OrderService service;

    @Autowired
    private OrderMapper orderMapper;

    private Order order;

    @Operation(summary = "Order insert operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Order inserted successfully"),
            @ApiResponse(responseCode = "400", description = "Missing required fields")
    })
    @PostMapping
    public ResponseEntity<OrderDTO> insert (@Valid @RequestBody OrderDTO orderDTO){
        Order orderInsert = service.insert(orderDTO);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(orderMapper.mapToDto(orderInsert));
    }

    @Operation(summary = "Return Order found by id operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order found successfully"),
            @ApiResponse(responseCode = "404", description = "Order does not exist in the system")
    })
    @GetMapping(value = "/{id}")
    public OrderDTO findById (@PathVariable Long id) throws OrderNotFoundException {
        Order result = service.findById(id)
                .orElseThrow(() -> new OrderNotFoundException(id));
        return orderMapper.mapToDto(result);
    }

    @Operation(summary = "Update Order status operation")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Order changed successfully"),
            @ApiResponse(responseCode = "400", description = "Order exception"),
            @ApiResponse(responseCode = "404", description = "Order does not exist in the system")
    })
    @PutMapping(value = "/{id}/{status}")
    public OrderDTO update (@PathVariable Long id, @PathVariable OrderStatusEnum status) throws OrderNotFoundException {
        Order orderUpdate = service.update(id, status);
        return orderMapper.mapToDto(orderUpdate);
    }

}
