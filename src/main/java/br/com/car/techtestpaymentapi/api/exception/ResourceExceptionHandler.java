package br.com.car.techtestpaymentapi.api.exception;

import br.com.car.techtestpaymentapi.exception.OrderException;
import br.com.car.techtestpaymentapi.exception.OrderNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ResourceExceptionHandler {

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<ErrorResponse> orderNotFound(OrderNotFoundException ex, HttpServletRequest request) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ErrorResponse(System.currentTimeMillis(),
                        HttpStatus.NOT_FOUND.value(),
                        "Order Not Found",
                        ex.getMessage(),
                        request.getRequestURI()));
    }

    @ExceptionHandler(OrderException.class)
    public ResponseEntity<ErrorResponse> invalidStatus (OrderException ex, HttpServletRequest request) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(new ErrorResponse(System.currentTimeMillis(),
                        HttpStatus.BAD_REQUEST.value(),
                        "Order Exception",
                        ex.getMessage(),
                        request.getRequestURI()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> validation(MethodArgumentNotValidException ex, HttpServletRequest request) {
        ErrorResponse err = new ErrorResponse(System.currentTimeMillis(),
                HttpStatus.BAD_REQUEST.value(),
                "Validation Exception",
                ex.getBindingResult().getObjectName(),
                request.getRequestURI());

        for (FieldError x : ex.getBindingResult().getFieldErrors()) {
            err.addErrorDetail(new ErrorDetail(x.getField(), x.getDefaultMessage()));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
    }

}
